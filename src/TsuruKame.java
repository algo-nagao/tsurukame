import java.util.Scanner;

public class TsuruKame {
	// 力づく法による鶴の数の求め方
	static int Tsuru1(int n, int m) {
		for (int i = 0; i <= n; i++) {
			int f = 2 * i + 4 * (n - i); // 足の数
			if (f == m)
				return i;
		}
		return -1;
	}

	/**
	 * 連立方程式による鶴の数の求め方
	 * 
	 * @param n 鶴亀の総数
	 * @param m 足の総数
	 * @return 鶴の数
	 */
	static int Tsuru2(int n, int m) {
		// TODO
		return 0;
	}

	public static void main(String[] args) {
		Scanner stdIn = new Scanner(System.in);

		System.out.print("　鶴亀の匹数：");
		int n = stdIn.nextInt();
		System.out.print("　鶴亀の足数：");
		int m = stdIn.nextInt();
		System.out.println("鶴の数は" + Tsuru1(n, m) + "です");
		System.out.println("鶴の数は" + Tsuru2(n, m) + "です");
		stdIn.close();
	}

}

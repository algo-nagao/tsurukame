import static org.junit.Assert.*;
import org.junit.Test;

public class TsuruKameTest {
	@Test
	public void Tsuru1() {
		assertEquals(2, TsuruKame.Tsuru1(5, 16));
		assertEquals(3, TsuruKame.Tsuru1(5, 14));
		assertEquals(4, TsuruKame.Tsuru1(5, 12));
		assertEquals(5, TsuruKame.Tsuru1(5, 10));
	}

	@Test
	public void Tsuru2() {
		assertEquals(2, TsuruKame.Tsuru2(5, 16));
		assertEquals(3, TsuruKame.Tsuru2(5, 14));
		assertEquals(4, TsuruKame.Tsuru2(5, 12));
		assertEquals(5, TsuruKame.Tsuru2(5, 10));
	}
}
